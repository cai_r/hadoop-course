#! /bin/bash

blue=$(tput setaf 2)
normal=$(tput sgr0)

order="0 1 2"
nb=(1 2 5)

declare -A dict

for i in $order; do
  name="hadoop@r06p0${nb[$i]}"
  echo "${blue}$name${normal}"

  # copy files on machines
  if [ ! $(ssh $name ls "folder/S$i.txt" 2> /dev/null) ]; then
    echo "File S$i.txt doesn't exist: copying"
    scp S$i.txt $name:folder/S$i.txt
  fi

  # map to UM
  if [ ! $(ssh $name ls "folder/UM_$i.txt" 2> /dev/null) ]; then
    echo "map to UM"
    ssh $name bash folder/map.sh folder/S$i.txt
  fi

  # show keys
  if true; then
    keys=$(ssh $name bash folder/show_key.sh folder/UM_$i.txt)
    for k in $keys; do
      dict[$k]="${dict[$k]} UM_$i.txt"
    done
  fi

  # ls on multi computers
  if true; then
    echo "$(ssh $name ls folder/)"
  fi

  # prepare for reduce
  if [ $(ssh $name ls "folder/keys" 2> /dev/null) ]; then
    ssh $name rm folder/keys
  fi

done

# show list of key - UMs pairs
for k in "${!dict[@]}"; do
  echo clé $k - liste d\'UMs: ${dict[$k]}
done

index=0
# copy files (shuffle)
for k in "${!dict[@]}"; do
  n=${nb[$index]}
  name="hadoop@r06p0$n"
  for target in ${dict[$k]}; do
    num=${target##UM_}
    num=${num%%.txt}
    scp hadoop@r06p0${nb[$num]}:folder/$target tmp
    scp tmp $name:folder/$target
  done

  ssh $name "echo $k >> folder/keys"
  index=$(( ($index + 1) % ${#nb[@]} ))
done

# reduce
for i in $order; do
  name="hadoop@r06p0${nb[$i]}"
  for k in $(ssh $name cat folder/keys); do
    ssh $name bash folder/reduce.sh $k RM_$i.txt ${dict[$k]}
  done

done
