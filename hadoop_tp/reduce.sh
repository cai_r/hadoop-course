#! /bin/sh

# at least 2 parameters expected - key and UM-files
if [ "$#" -lt 3 ]; then
  exit 1
fi

key=$1
out=folder/$2
shift 2
val=0

for file in "$@"; do
  val=$(($val + $(cat folder/$file | grep -w "$key" | wc -l)))
done

echo $key $val >> $out
