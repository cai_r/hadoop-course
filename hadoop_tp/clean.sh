#! /bin/sh

nb="1 2 5 7"

for i in $nb; do
  name="hadoop@r06p0$i"
  if [ $(ssh $name rm -rf folder 2> /dev/null) ]; then
    echo "$name: folder removed"
  fi
done
