#! /bin/bash

nb="1 2 5 7"
prog="map show_key reduce"

for i in $nb; do
  name="hadoop@r06p0$i"
  echo "$name"
  ssh $name "if [ ! -d folder ]; then
                mkdir folder
             fi"

  for p in $prog; do
    scp $p.sh $name:folder/$p.sh
    ssh $name chmod +555 folder/$p.sh
  done
done
