#! /bin/sh

unset keys
declare -A keys

filename=$1

words=$(cut -d' ' -f1 $filename)

for word in $words; do
  keys[$word]=1
done

for key in "${!keys[@]}"; do
  echo $key
done
