#! /bin/sh

filename=$1
IFS="$'\n' "
out=folder/UM_${filename##*/S}

for word in `cat $filename`; do
    echo "$word 1" >> $out
done
